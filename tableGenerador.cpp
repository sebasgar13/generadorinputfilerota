#include <string>
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

class RowTable {
    public:
        string fileName;
        double value;    
        RowTable(string fileNameInp){
            fileName = fileNameInp;
        }
};

class Table {
	public:
 		vector<RowTable> rowsTable;
		double minV; 
		Table(string fileInput = "energias.dat"){
    		ifstream MyFile(fileInput);
			string lineTextFile;
			bool isNameFile = true;
			int contadorLinesIndex = 0;

            cout.precision(9);
		    while (getline (MyFile, lineTextFile)){
        		//cout << lineTextFile << endl;
		        if(isNameFile){
    		        rowsTable.push_back(RowTable(lineTextFile));
        		} else {
					if(contadorLinesIndex == 0){
						minV = stod(lineTextFile);
					} else if(stod(lineTextFile) < minV){
						minV = stod(lineTextFile);
						indexMin = contadorLinesIndex;
					}
	            	rowsTable[contadorLinesIndex].value = stod(lineTextFile);
    		        contadorLinesIndex++;
        		}
	        	isNameFile = !isNameFile;
    		}
    		MyFile.close();
			if(indexMin != 0) sort();
		}

		int size(){
			return rowsTable.size();
		}

		void generateTableGNP(){
            string nameOut = outFile == "" ? "tableGNP.dat" : outFile + "GNP.dat";
            float increment = 360.0/size();
            ofstream MyFile(nameOut);
            MyFile.precision(9);
            MyFile << "#NameFile \t Theta \t Energy(Hartree) \t RelativeEnergy(Hartree) \n"<< fixed;
            for(int i = 0; i < size(); i++){
                MyFile << rowsTable[i].fileName + "\t " << i*increment << "\t" << rowsTable[i].value << "\t" << rowsTable[i].value - minV << "\n"  ;
            } 
            MyFile << rowsTable[0].fileName + "\t " << 360.0 << "\t" << rowsTable[0].value << "\t" << rowsTable[0].value - minV << "\n"  ;

            MyFile.close();
		}

		void generateFileInpHindRotor(){
            string nameOut = outFile == "" ? "tableHindRotor.dat" : outFile + "HindRotor.dat";
            float increment = 360.0/size();
            ofstream MyFile(nameOut);

            MyFile.precision(9);
            MyFile << fixed;
            for(int i = 0; i < size(); i++){
                MyFile << i*increment << "\t" << rowsTable[i].value << "\n"  ;
            }
            MyFile << 360.0 << "\t" << rowsTable[0].value << "\n";            
            MyFile.close();
		}

        void setNameOutFile(string name){
            outFile = name;
        }

	private:
		int indexMin;
        string outFile = "";
		void sort(){
			vector<RowTable> rowsTableOrden;
			for(int i = indexMin; i < rowsTable.size(); i++){
				rowsTableOrden.push_back(rowsTable[i]);
			}
			for(int i = 0; i < indexMin; i++){
				rowsTableOrden.push_back(rowsTable[i]);
			}
			rowsTable = rowsTableOrden;
		}
};

int main(int argc, char* argv[]){
	Table generationTable = Table();
    generationTable.setNameOutFile("salida");
    generationTable.generateTableGNP();
    generationTable.generateFileInpHindRotor();
}
