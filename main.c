#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int retornaUno();

void agregarMetodo(FILE *salidaFile);
void regresarDih(int numero, int incremento, char *diedro, char *respuesta);
void handleError(int *error, char *mensajeError, char escritura[1000]){
	*error = 1;
	strcpy(mensajeError, escritura);
} 

int main(int argc, char *argv[]){

	FILE *geometriaInp = NULL;
	FILE *salida = NULL;
	char diedros[4][6];
    char outFile[1000] = "salida";
	char lineasAGrabar[1000][1000];
	int incremento = 30;
	
	int dihLabel = 0;

	int error = 0;
	char mensajeError[1000];

	char caracter[9];
	char linea[1000] = "";

	char nombreArchivo[100] = "./";
	char nombre[100] = "prueba2.txt";
	
	//El nombre del archivo es el primer parametro
	if(argc > 3){
		strcpy(nombre, argv[1]);		
		strcat(nombreArchivo, nombre);
		geometriaInp = fopen(nombreArchivo, "r");
		if(geometriaInp){
			
			//To do. Validar la existencia de la bandera de los diedros y cuantos diedros son
			for(int i = 1; i < argc; i++){
				if(strstr(argv[i], "-d")){
					if(dihLabel == 0){
						if(i+1 >= argc){
							handleError(& error, mensajeError, "Estructura de entrada no valida: \n Seguido de la bandera -d debe de existir un argumento que haga referencia a un ángulo diedro \n");
							printf("eee %d \n", error);
							break;
						}
						if(strstr(argv[i+1], "dih")){
							//Registra el primer ángulo diedro
							dihLabel += 1;
							strcpy(&diedros[0][0],argv[i+1]);
							i +=1;
						} else {
							handleError( &error, mensajeError, 
									"Estructura de entrada no valida: \n Seguido de la bandera -d debe de existir un argumento que haga referencia a un ángulo diedro \n");
							break;
						}
				
						//Detectar los demas argumento de ángulos diedros
						for(int j = 1; j < 4; j++){
							if(  (i+j+1) < argc && strstr(argv[i+j], "dih")){
								dihLabel +=1;
								strcpy(&diedros[j][0], argv[i+j]);
								printf("\t \t %s \n", argv[i+j]);	
							} else {
								i = i + j - 1;
								break;
							}
						}
					}
				} else if(strstr(argv[i], "-a")) {
					if(i+1 < argc && atoi(argv[i+1]) > 0){
						incremento = atoi(argv[i+1]);
					} else {
						//Error
                        printf("error : %s \n\n", argv[i+1]);
						error = 1;
						strcpy(mensajeError, "Estructura de entrada no valida: \n despues de -a sigue un número entero positivo \n");
						break;
					}
					i += 1; 
				} else if(strcmp(argv[i], "-o") == 0){
                    if(i+1 < argc){
						strcpy(outFile, argv[i+1]);
                        i += 1;
					}
                }
			}
			if(error == 1){
				printf("%s", mensajeError);
			} else {
                printf("%d \n \n", dihLabel);
				for(int i = 0; i < dihLabel; i++){
					printf("Diedro: %s \n", &diedros[i][0]);
				}
			

				int lineas = 0;
				int countSaltos = 0;
				while(1){
					int controlDeEscritura = 0;
					fgets(linea, 1000, geometriaInp);
					strncpy(caracter, linea, 6);
					if(strstr(caracter, " \n")){
						if(countSaltos <= 1){
							countSaltos += 1;
						} else {
							controlDeEscritura = 1;
						}
					} else if(strstr(caracter, "dih")) {
						for(int j = 0; j < dihLabel; j++){
							if(strstr(caracter, &diedros[j][0])){
								controlDeEscritura = 1;
								break;
							} 
						}
					}

					if(controlDeEscritura == 0){
						//TODO: Grabar todas las lineas excepto las de los diedros
						strcpy(&lineasAGrabar[lineas][0], linea);
						printf("%d %s", lineas, linea);
						lineas++;
					}
					if(feof(geometriaInp)){
						break;
					}
				}
				char archivoNombreSalida[1000] = "salida.txt"; 

                for(int i = 0; i < 360; i+=15){
                    char numero = i;
                    char cadena[5];
                    char lineaGenerada[100];

                    strcpy(archivoNombreSalida, outFile);
                    sprintf(cadena, "%d", i);
                    if(i < 100){
                        char backup[5];
                        strcpy(backup, cadena);
                        if(i < 10){
                            strcpy(cadena, "00");
                            strcat(cadena, backup);
                        } else {
                            strcpy(cadena, "0");
                            strcat(cadena, backup);
                        }            
                    }
                    strcat(archivoNombreSalida, cadena);
                    strcat(archivoNombreSalida, ".inp");
                    printf("%s \n", archivoNombreSalida);
                    salida = fopen(archivoNombreSalida, "w");
                	if(salida != NULL){			
                        agregarMetodo(salida);
				    	for(int i = 0; i < lineas; i++){
						    fputs( &lineasAGrabar[i][0], salida);
					    }
                        regresarDih(1, i, diedros[0], lineaGenerada);
                        fputs(lineaGenerada, salida);
                        fputs("\n\n\n", salida);
    				} else {
	    				printf("salida mal \n");
		    		}

                    fclose(salida);
                }
							
				printf("El número de líneas en el archivo son: %d", lineas);
			}
			fclose(geometriaInp);
		} else {
			printf("No fue posible abrir el archivo \n");
		}
	}
}
