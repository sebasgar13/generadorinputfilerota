
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void agregarMetodo(FILE *salidaFile){
   fputs("%chk=singleDAScanp-0.chk\n", salidaFile); 
   fputs("%mem=60GB\n", salidaFile);
   fputs("%nproc=12\n", salidaFile);
   fputs("#B3LYP/6-311g(2d,d,p) opt=z-mat GFInput IOP(6/7=3) test \n\n", salidaFile);
   fputs("Comentario \n\n", salidaFile);
   fputs("0 1\n", salidaFile);
}

void regresarDih(int numero, int incremento, char *diedro, char *respuesta){
    float aumento = numero * incremento;
    char aumentoC[20];

    strcpy(respuesta, diedro);
    strcat(respuesta, "\t");
    sprintf(aumentoC, "%f", aumento);
    strcat(respuesta, aumentoC);
}

